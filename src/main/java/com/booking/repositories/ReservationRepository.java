package com.booking.repositories;

import java.util.ArrayList;
import java.util.List;


import com.booking.models.Reservation;

public class ReservationRepository {
    private static List<Reservation> listReservation = new ArrayList<>();
    private static int orderCount = 0;

    public static List<Reservation> getAllReservation() {
        return listReservation;
    }

    public static String getNextOrderId(){
        orderCount++;
        return "Rsv-" + String.format("%02d", orderCount);
    }
}
