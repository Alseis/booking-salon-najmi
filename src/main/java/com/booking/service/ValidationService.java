package com.booking.service;

import java.util.List;

import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Person;

public class ValidationService {
    // Buatlah function sesuai dengan kebutuhan
    public static void validateInput(){

    }

    public static Customer validateCustomerId(String id, List<Person> listPerson){
        for (Person person : listPerson) {
            if (person instanceof Customer && person.getId().equals(id)) {
                return (Customer) person;
            }
        }
        return null;
    }

    public static Employee validateEmployeeId(String id, List<Person> listPerson){
        for (Person person : listPerson) {
            if(person instanceof Employee && person.getId().equals(id)){
                return (Employee) person;
            }
        }
        return null;
    }

    
}
