package com.booking.service;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Person;
import com.booking.models.Reservation;
import com.booking.models.Service;

public class PrintService {
    public static void printMenu(String title, String[] menuArr){
        int num = 1;
        System.out.println(title);
        for (int i = 0; i < menuArr.length; i++) {
            if (i == (menuArr.length - 1)) {   
                num = 0;
            }
            System.out.println(num + ". " + menuArr[i]);   
            num++;
        }
    }

    public String printServices(List<Service> serviceList){
        String result = "";
        // Bisa disesuaikan kembali
        for (int i = 0; i < serviceList.size(); i++) {
            Service service = serviceList.get(i);
            result += i == serviceList.size() - 1 ? service.getServiceName() : service.getServiceName() + ", ";
        }
        return result;
    }

    // Function yang dibuat hanya sebgai contoh bisa disesuaikan kembali
    public void showRecentReservation(List<Reservation> reservationList){
        AtomicInteger num = new AtomicInteger(1);

        System.out.println("+------+------------+---------------+--------------------------------------------------------------+-----------------+-----------------+------------+");
        System.out.printf("| %-4s | %-10s | %-13s | %-60s | %-15s | %-15s | %-10s |\n",
                "No.", "ID", "Nama Customer", "Service", "Biaya Service", "Pegawai", "Workstage");
        System.out.println("+------+------------+---------------+--------------------------------------------------------------+-----------------+-----------------+------------+");
        reservationList.stream()
                        .filter(reserv -> reserv.getWorkstage().equalsIgnoreCase("Waiting") || reserv.getWorkstage().equalsIgnoreCase("In process"))
                        .forEach(reservation -> {
                            System.out.printf("| %-4s | %-10s | %-13s | %-60s | %-15s | %-15s | %-10s |\n",
                            num.getAndIncrement(), reservation.getReservationId(), reservation.getCustomer().getName(), printServices(reservation.getServices()), reservation.getReservationPrice(), reservation.getEmployee().getName(), reservation.getWorkstage());
                        });
        System.out.println("+------+------------+---------------+--------------------------------------------------------------+-----------------+-----------------+------------+");
    }

    public void showAllCustomer(List<Person> listCustomer){
        AtomicInteger num = new AtomicInteger(1);
    
        System.out.println("+------+------------+-------------+-----------------+-----------------+-----------------+");
        System.out.printf("| %-4s | %-10s | %-11s | %-15s | %-15s | %-15s |\n",
                "No.", "ID", "Nama", "Alamat", "Membership", "Uang");
        System.out.println("+------+------------+-------------+-----------------+-----------------+-----------------+");
        listCustomer.stream()
                    .filter(cust -> cust instanceof Customer)
                    .map(person -> (Customer)person)
                    .forEach(customer -> {
                        System.out.printf("| %-4s | %-10s | %-11s | %-15s | %-15s | %-15s |\n",
                        num.getAndIncrement(), customer.getId(), customer.getName(), customer.getAddress(), customer.getMember().getMembershipName(), customer.getWallet());
                    });
        System.out.println("+------+------------+-------------+-----------------+-----------------+-----------------+");
    }

    public void showAllEmployee(List<Person> listEmployee){
        AtomicInteger num = new AtomicInteger(1);
    
        System.out.println("+------+------------+-------------+-----------------+-----------------+");
        System.out.printf("| %-4s | %-10s | %-11s | %-15s | %-15s |\n",
                "No.", "ID", "Nama", "Alamat", "Pengalaman");
        System.out.println("+------+------------+-------------+-----------------+-----------------+");
        listEmployee.stream()
                    .filter(emp -> emp instanceof Employee)
                    .map(employees -> (Employee)employees)
                    .forEach(employee -> {
                        System.out.printf("| %-4s | %-10s | %-11s | %-15s | %-15s |\n",
                        num.getAndIncrement(), employee.getId(), employee.getName(), employee.getAddress(), employee.getExperience());
                    });
        System.out.println("+------+------------+-------------+-----------------+-----------------+");
    }

    public void showHistoryReservation(List<Reservation> reservationList){
        AtomicInteger num = new AtomicInteger(1);
        AtomicInteger keuntungan = new AtomicInteger(0);

        System.out.println("+------+------------+---------------+--------------------------------------------------------------+-----------------+-----------------+------------+");
        System.out.printf("| %-4s | %-10s | %-13s | %-60s | %-15s | %-15s | %-10s |\n",
                "No.", "ID", "Nama Customer", "Service", "Biaya Service", "Pegawai", "Workstage");
        System.out.println("+------+------------+---------------+--------------------------------------------------------------+-----------------+-----------------+------------+");
        reservationList.stream()
                        .forEach(reservation -> {
                            System.out.printf("| %-4s | %-10s | %-13s | %-60s | %-15s | %-15s | %-10s |\n",
                            num.getAndIncrement(), reservation.getReservationId(), reservation.getCustomer().getName(), printServices(reservation.getServices()), reservation.getReservationPrice(), reservation.getEmployee().getName(), reservation.getWorkstage());
                            if(reservation.getWorkstage().equalsIgnoreCase("Finish")){
                                double price = reservation.getReservationPrice();
                                keuntungan.addAndGet((int) price);
                            }
                        });
        System.out.println("+--------------------------------------------------------------------------------------------------------------------+------------------------------+");
        System.out.printf("| %-114s | %-28s |\n",
                            "Total keuntungan", "Rp " + keuntungan);
        System.out.println("+--------------------------------------------------------------------------------------------------------------------+------------------------------+");
    }

    public void showAllServices(List<Service> serviceList){
        AtomicInteger num = new AtomicInteger(1);
    
        System.out.println("+------+------------+----------------------+-----------------+");
        System.out.printf("| %-4s | %-10s | %-20s | %-15s |\n",
                "No.", "ID", "Nama", "Harga");
        System.out.println("+------+------------+----------------------+-----------------+");
        serviceList.stream()
                    .forEach(service -> {
                        System.out.printf("| %-4s | %-10s | %-20s | %-15s |\n",
                        num.getAndIncrement(), service.getServiceId(), service.getServiceName(), service.getPrice());
                    });
        System.out.println("+------+------------+----------------------+-----------------+");
    }
}
